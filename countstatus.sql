drop procedure if exists CountOrdersByStatus;
DELIMITER $$

create PROCEDURE CountOrdersByStatus(in OrdersStatus varchar(50),out Total INT)
BEGIN
	select count(orderNumber)
	into Total
	from orders where status=OrdersStatus;
END $$

DELIMITER ;
