
drop procedure if exists StatusByCustomer;
DELIMITER $$

create PROCEDURE StatusByCustomer(in orderNumber INT,in CustomerNumber INT,out OrderStatus varchar(15))
BEGIN
        select status
	into OrderStatus
        from orders where orderNumber=OrderNumber and customerNumber = CustomerNumber
	LIMIT 1;
END $$

DELIMITER ;

